# IKE Office API Quickstart guide

This guide will need a REST Client. We recommend using either [Postman](https://www.getpostman.com) or [Insomnia](https://insomnia.rest).

You will need two things to query the data:

1. All API endpoints (aside from login) require an "access token" to authenticate.
2. Most API endpoints require a "department Id" to specify which department to query data from.

## Creating access tokens

To use the IKE Office API, you need an access token. To create a token, you will need to login using your IKE ID credentials.

### Create a POST request with the following parameters

- URL: `https://login.ikegps.com/oauth/token`
- Request: `POST`
- Content-Type: `application/x-www-form-urlencoded`

FormData format:

| Name | Value |
| - | - |
| grant_type | `password` |
| client_id | `eByTHyjFcmwdRXwqjM4X2CKsyx9lRe9t` |
| audience | `https://office.ikegps.com/v1/` |
| username | `Your-IKE-ID-Email` |
| password | `Your-IKE-ID-Password` |

```bash
curl --request POST \
  --url https://login.ikegps.com/oauth/token \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data grant_type=password \
  --data client_id=eByTHyjFcmwdRXwqjM4X2CKsyx9lRe9t \
  --data audience=https://office.ikegps.com/v1/ \
  --data username=Your-IKE-ID-Email \
  --data password=Your-IKE-ID-Password
```

#### Your access token will be returned in the response

```json
{
  "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2xvZ2luLmlrZWdwcy5jb20vIiwic3ViIjoiYXV0aDB8V2h5QXJlWW91RGVjb2RpbmdUaGlzVG9rZW5XaGVuIFlvdUNvdWxkQmVQbGF5aW5nRm9vc2JhbGwiLCJhdWQiOiJodHRwczovL29mZmljZS5pa2VncHMuY29tL3YxLyIsImlhdCI6MTY2MjA3MzY0NSwiZXhwIjoxNjYyMDgxNjQ1LCJhenAiOiJ0aGVDbGllbnRJZE9mVGhlUmVxdWVzdCIsImd0eSI6InBhc3N3b3JkIn0.5vnP1ex5w7zzGuaMuKCpvsrAuZ5ECQnh7tWk5eZY0E8",
  "expires_in": 86400, // Token duration in milliseconds
  "token_type": "Bearer"
}
```

## Using access tokens

To use these access tokens, they must be passed into the `Header` of the request using the following format:

| Header Name | Header Value |
| - | - |
| Authorization | `Bearer !!The-Access-Token-You-Got-From-Above!!` |

### Using your access token, fetch your user information

- Url: `https://office.ikegps.com/v1/me.json`
- Request: `GET`
- Headers:

| Header Name | Header Value |
| - | - |
| Authorization | `Bearer !!The-Access-Token-You-Got-From-Above!!` |

#### Response in JSON

```json
{
  "schema": "https://office.ikegps.com/v1/schema/user.json",
  "id": "sampleId",
  "url": "https://office.ikegps.com/v1/user/sampleId.json",
  "revision": {
    "id": "sampleRevision",
    "url": "https://office.ikegps.com/v1/user/sampleId/sampleRevision.json"
  },
  "email": "sample@user.com",
  "createdAt": "2017-10-10T01:55:13.356Z",
  "updatedAt": "2018-04-12T23:32:14.959Z",
  "firstName": "Sample",
  "lastName": "User",
  "settings": {
    "units": "feet",
    "dateFormat": "MM/DD/YYYY",
    "department": {
      "id": "AKDn7fTosl",
      "url": "https://office.ikegps.com/v1/department/AKDn7fTosl.json"
    }
  },
  "sso_id": "ssoProvider|ssoSampleUserId"
}
```

### Finding your departments

After you have an access token, you will need to know which departments are available to query from. If you have a current department it is provided in the login response above as `settings.department.id`. However, you can also list all of your departments.

- URL: `https://office.ikegps.com/v1/department.json`
- Request: `GET`
- Headers:

| Header Name | Header Value |
| - | - |
| Authorization | `Bearer !!The-Access-Token-You-Got-From-Above!!` |

#### Response

```json
[
  {
    "name": "Sample Department 1",
    "id": "AKDn7fTosl",
    "url": "https://office.ikegps.com/v1/department/AKDn7fTosl.json"
  },
  {
    "name": "Sample Department 2",
    "id": "UVTDwX05Hj",
    "url": "https://office.ikegps.com/v1/department/UVTDwX05Hj.json"
  },
]
```

### Listing your jobs

To get a list of your jobs, you will need to make a GET request to `https://office.ikegps.com/v1/job.json`

#### Create a GET request with the following parameters

- URL: `https://office.ikegps.com/v1/job.json?departmentId=AKDn7fTosl`
- Request: `GET`
- Headers:

| Header Name | Header Value |
| - | - |
| Authorization | `Bearer !!The-Access-Token-You-Got-From-Above!!` |

#### A list of all your jobs will be returned in the response

```json
[{
    "schema": "https://office.ikegps.com/v1/schema/job.json",
    "id": "aTelTA5lK1",
    "url": "https://office.ikegps.com/v1/job/aTelTA5lK1.json",
    "name": "1st Street",
    "createdAt": "2016-05-16T15:40:31.385Z",
    "updatedAt": "2016-05-16T15:41:36.748Z"
}, {
    "schema": "https://office.ikegps.com/v1/schema/job.json",
    "id": "az67o3qnBa",
    "url": "https://office.ikegps.com/v1/job/az67o3qnBa.json",
    "name": "Main Road",
    "createdAt": "2016-05-05T01:40:09.115Z",
    "updatedAt": "2016-05-05T01:41:19.078Z"
}]
```

The Job response contains information about how to get more information about the specific job.

- `url` A link to more information about the job
- `schema` A link to a JSONSchema for what the job's JSON output format will look like

### Listing your collections

To get a list of your collections, you will need to make a GET request to `https://office.ikegps.com/v1/collection.json`

#### Create a GET request with the following parameters

- URL: `https://office.ikegps.com/v1/collection.json?departmentId=AKDn7fTosl`
- Request: `GET`
- Query Parameters:
  - `jobId` = `aTelTA5lK1`
- Headers:

| Header Name | Header Value |
| - | - |
| Authorization | `Bearer !!The-Access-Token-You-Got-From-Above!!` |

##### A list of collections belonging to that job will be returned in the response

This response will be very large as it contains all the collected information and details about all the measurements collected.

```json
[{
    "schema": "https://office.ikegps.com/v1/schema/collection.json",
    "id": "K7e8FiWeez",
    "url": "https://office.ikegps.com/v1/collection/K7e8FiWeez.json",
    "revision": {
      "id": "J6ytT6Jez",
      "url": "https://office.ikegps.com/v1/collection/K7e8FiWeez/J6ytT6Jez.json"
    },
    "job": {
      "id": "aTelTA5lK1",
      "url": "https://office.ikegps.com/v1/job/aTelTA5lK1.json"
    },
    "form": {
      "id": "OmuMwXURL",
      "url": "https://office.ikegps.com/v1/form/OmuMwXURL.json"
    },

    // ...

    "captures": [{
        "type": "truesize",
        "id": "Ari12zCNIa",
        "url": "",
        "imageUrl": "https:// ... /Ari12zCNIa.jpeg",
        "thumbnailUrl": "https:// ... /Ari12zCNIa_thumbnail.jpeg",
        "compositeUrl": "https:// ... /Ari12zCNIa_composite_meters.jpeg",
        "createdAt": "2016-05-16T15:41:23.124Z",
        "updatedAt": "2016-06-03T04:35:25.202Z",

        // ...
    }]
}]
```

The response contains more URLs to find more information about the collection and it's captures:

- `schema` A link to the JSONSchema for the collection response
- `form` A link to the JSON form object that the collection was collected against
- `captures` A list of all captures used inside of the collection this includes
  - `captures`.`imageUrl` Url to the Raw image that was captured
  - `captures`.`thumbnailUrl` Url to the thumbnailUrl
  - `captures`.`compositeUrl` Url to the composite image (Raw image with the measurements overlaid.)

The composite image can also be rendered with different measurement units. To switch measurement unit, change the image filename in the url

- Meters ``/Ari12zCNIa_composite_meters.jpeg``
- Feet ``/Ari12zCNIa_composite_feet.jpeg``

## Further Information

Documentation for all the existing endpoints can be found at ``https://bitbucket.org/ikegps/office-api``

If you have any questions please email ``support@ikegps.com``
