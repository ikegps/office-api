# IKE Office API v1

## API Urls

All urls will start with the endpoint : ``https://office.ikegps.com/v1``
All responses will have the ContentType ``application/json``
All data will be sent and received in ``JSON``

### User

- ``GET: /me.json``
- ``GET: /user.json``
- ``GET: /user/:userId.json``

### Department

- ``GET: /department.json``
- ``GET: /department/:departmentId.json``

### Form

[JSON Schema](https://office.ikegps.com/v1/schema/form.json)

- ``GET: /form/:formId.json``
- ``GET: /form/:formId/:revision.json``

### Job

[JSON Schema](https://office.ikegps.com/v1/schema/job.json)

- ``GET: /job.json?departmentId=:departmentId``
- ``GET: /job/:jobId.json``

### Collection

[JSON Schema](https://office.ikegps.com/v1/schema/collection.json)

- ``GET: /collection.json?jobId=:jobId&departmentId=:departmentId``
- ``GET: /collection/:collectionId.json``
- ``GET: /collection/:collectionId/:revision.json``

### Capture

- ``GET: /collection/:collectionId/:revision/:captureId.jpeg``
- ``GET: /collection/:collectionId/:revision/:captureId_thumbnail.jpeg``
- ``GET: /collection/:collectionId/:revision/:captureId_composite.jpeg``
- ``GET: /collection/:collectionId/:revision/:captureId_composite_feet.jpeg``
- ``GET: /collection/:collectionId/:revision/:captureId_composite_meters.jpeg``
- ``GET: /collection/:collectionId/:revision/:captureId_measurements.png``
- ``GET: /collection/:collectionId/:revision/:captureId_measurements_feet.png``
- ``GET: /collection/:collectionId/:revision/:captureId_measurements_meters.png``

## Response Codes

### 400

When the request could not be parsed as JSON.

```txt
HTTP/1.1 400 Bad Request
{
    "message": "Invalid JSON"
}
```

### 422

When a request fails due to invalid parameters

```txt
HTTP/1.1 422 Unprocessable Entity
{
    "message": "Validation failed"
    "errors" : [
        {
            "field": "id",
            "codes": "already_exists"
        }
    ]
}
```

#### Error codes

|Code|Description|
|-------|----|
|missing| Resources does not exist |
|missing_field| Required field was not provided |
|invalid| Field is invalid|
|already_exists| A unique value constraint was violated|
|custom| Custom error message, this object will contain a ``message`` parameter|

## Data types

### Date/Time

- All timestamps are ISO ``YYYY-MM-DDTHH:MM:SSZ``
- All timestamps are in UTC

### Distances/Measurements

- All distances are in Meters.

### Angles

- All angles are in Radians.

## Authentication

All requests need to be authorized via a access token.

### Generating access tokens

To generate a token, see the `Creating access tokens` section in the [README.md](./README.md#creating-access-tokens) file.

### Using access tokens

To use your access token, see the `Using access tokens` section in the [README.md](./README.md#using-access-tokens) file.
